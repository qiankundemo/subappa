// import Vue from "vue";
// import VueRouter from "vue-router";

import Home from "./views/Home";
import About from "./views/About";

// Vue.use(VueRouter);

export default [
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/about",
    component: About,
  },
];

// export default new VueRouter({
//     base: window.__POWERED_BY_QIANKUN__ ? '/app-vue/' : '/',
//     mode: 'hash',
//   routes,
// });
